// Importing libraries
const express = require("express");
const dotenv = require("dotenv").config();
const cors = require("cors");
const bodyParser = require("body-parser");
const os = require("os");
const sql = require("mssql/msnodesqlv8");

const app = express();

// Using cors middleware
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// DB Connection
const config = {
  // user: process.env.DB_USER,
  // password: process.env.DB_PASSWORD,
  server: process.env.SERVER_NAME,
  database: process.env.DB_NAME,
  schema: process.env.DB_SCHEMA,
  port: 63492,
  options: {
    encrypt: false, // for azure
    trustedConnection: true,
    useUTC: true,
  },
};

sql.connect(config, function (err) {
  if (err) console.log(err);

  console.log("DB connected");
});

var request = new sql.Request();

app.get("/warranty/manager/api/find/sku/details/:sku", (req, res) => {
  const { sku } = req.params;

  const skuDetailsQuery = `SELECT [XSKU], L.TierID, [ActiveReport], [ExchangeStart], 
    [OperatingSystem], [InsertDate], [ActiveReport2], [LaunchDate], [OEMRepairForecast], [Make], [Model]
    FROM [PSCO_dom].[dbo].[Warranty_XSKUList] L LEFT JOIN [PSCO].[dbo].[SKUManagement_SKUMaster] M
    ON L.XSKU = M.SKUX WHERE XSKU='${sku}'`;

  const newSkuDetails = `SELECT [Make], [Model]
  FROM [ODS].[dbo].[BW_Material] WHERE MaterialNumber= '${sku}' AND SKUType = 'WARRANTY'`;

  request.query(skuDetailsQuery, (err, data) => {
    if (err) console.log(err);

    if (data?.recordset.length) {
      res.status(200).json({
        data: data.recordset[0],
        isNewSku: false,
        meta: {
          retval: 1,
          servertime: Date.now(),
        },
      });
    } else {
      request.query(newSkuDetails, (err, data) => {
        if (err) {
          console.log(err);
        } else {
          if (data.recordset && data.recordset.length) {
            let newData = { ...data.recordset[0], XSKU: sku };
            res.status(200).json({
              data: newData,
              isNewSku: true,
              meta: {
                retval: 1,
                servertime: Date.now(),
              },
            });
          } else {
            res.status(200).json({
              data: data.recordset,
              meta: {
                retval: 2,
                servertime: Date.now(),
              },
            });
          }
        }
      });
    }
  });
});

app.get("/warranty/manager/api/preview/xsku/list", (req, res) => {
  let skuListQuery = `SELECT [XSKU], [TierID], [ActiveReport], [ExchangeStart], [UserID],
    [OperatingSystem], [InsertDate], [ActiveReport2], [LaunchDate], [OEMRepairForecast]
    FROM [PSCO_dom].[dbo].[Warranty_XSKUList]`;

  request.query(skuListQuery, (err, data) => {
    if (err) console.log(err);

    if (data?.recordset.length) {
      res.status(200).json({
        data: data.recordset,
        meta: {
          retval: 1,
          servertime: Date.now(),
        },
      });
    } else {
      res.status(200).json({
        data: data.recordset,
        meta: {
          retval: 2,
          servertime: Date.now(),
        },
      });
    }
  });
});

app.put("/warranty/manager/api/update/xsku", (req, res) => {
  const {
    tierId,
    operatingSystem,
    launchDate,
    exchangeStart,
    oemRepairForecast,
    activeReport,
    activeReportTwo,
    sku,
  } = req.body;

  let userId = `GSM1900\\${os.userInfo().username}`;

  let skuUpdateQuery = `UPDATE [PSCO_dom].[dbo].[Warranty_XSKUList] SET TierID = '${tierId}', OperatingSystem = '${operatingSystem}', ActiveReport = ${
    activeReport ? 1 : 0
  }, ActiveReport2 = ${activeReportTwo ? 1 : 0}, OEMRepairForecast = ${
    oemRepairForecast ? 1 : 0
  }, LaunchDate = '${launchDate}', ExchangeStart = '${exchangeStart}', UserID = '${userId}' WHERE XSKU = '${sku}'`;

  try {
    request.query(skuUpdateQuery, (err, data) => {
      if (err) console.log(err.message);

      res.status(200).json({
        data: null,
        meta: {
          retval: 1,
          servertime: Date.now(),
        },
      });
    });
  } catch (e) {
    console.log(e.message);
  }
});

app.post("/warranty/manager/api/insert/xsku", (req, res) => {
  const {
    make,
    model,
    tierId,
    operatingSystem,
    launchDate,
    exchangeStart,
    oemRepairForecast,
    activeReport,
    activeReportTwo,
    sku,
  } = req.body;
  let userId = `GSM1900\\${os.userInfo().username}`;

  const sqlAddXSKU = `INSERT INTO [PSCO_dom].[dbo].[Warranty_XSKUList] (XSKU,TierID,ActiveReport,ExchangeStart, OperatingSystem, UserID, ActiveReport2, LaunchDate, OEMRepairForecast, InsertDate) values (
    '${sku.trim()}',
    '${tierId}',
    ${activeReport ? "1" : "0"},
    '${exchangeStart}',
    '${operatingSystem}',
    '${userId}',
    ${activeReportTwo ? "1" : "0"},
    '${launchDate}',
    ${oemRepairForecast ? "1" : "0"},
    '${new Date().toISOString()}'
    )`;

  request.query(sqlAddXSKU, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({
        data: null,
        meta: {
          retval: 1,
          servertime: Date.now(),
        },
      });
    }
  });
});

const port = process.env.PORT || 3001;

// Listening to port
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
