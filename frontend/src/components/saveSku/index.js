import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Form, Card, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeftLong } from "@fortawesome/free-solid-svg-icons";
import _ from "lodash";
import app from "../common/app";
import config from "../common/config";
import swal from "sweetalert";

function SaveSku() {
  const location = useLocation();
  const navigate = useNavigate();
  let skuDetails = location.state?.skuDetails;
  const isNewSku = location.state?.isNewSku;
  const [state, setState] = useState({
    make: "",
    model: "",
    sku: "",
    operatingSystem: "",
    tierId: "",
    launchDate: "",
    exchangeStart: "",
    activeReport: false,
    activeReportTwo: false,
    oemRepairForecast: false,
  });

  useEffect(() => {
    if (!_.isEmpty(skuDetails)) {
      setState({
        make: skuDetails.Make,
        model: skuDetails.Model,
        sku: skuDetails.XSKU,
        operatingSystem: skuDetails.OperatingSystem
          ? skuDetails.OperatingSystem
          : "",
        tierId: skuDetails.TierID ? app.getTierName(skuDetails.TierID) : "",
        launchDate: app.getDate(skuDetails.LaunchDate),
        exchangeStart: app.getDate(skuDetails.ExchangeStart),
        oemRepairForecast: skuDetails.OEMRepairForecast
          ? skuDetails.OEMRepairForecast
          : false,
        activeReport: skuDetails.ActiveReport ? skuDetails.ActiveReport : false,
        activeReportTwo: skuDetails.ActiveReport2
          ? skuDetails.ActiveReport2
          : false,
      });
    }
  }, [skuDetails]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleCheckbox = (e) => {
    const { name, checked } = e.target;
    setState((prevState) => ({ ...prevState, [name]: checked }));
  };

  const updateDetails = async () => {
    state.tierId = state.tierId ? app.getTierId(state.tierId) : "";
    let payload = state;
    console.log(payload);
    if (isNewSku) {
      let resp = await app.insertSKUDetails(payload);
      if (resp.data.meta.retval === 1) {
        swal(`${state.sku} XSKU was uploaded successfully`, "", "success").then((value) =>
          navigate("/warrantysku")
        );
      }
    } else {
      let resp = await app.updateSKUDetails(payload);
      if (resp.data.meta.retval === 1) {
        swal("Records updated successfully", "", "success").then((value) =>
          navigate("/warrantysku")
        );
      }
    }
  };

  const handleSubmit = () => {
    let date = new Date();
    let currentTimeH = date.getHours();
    let currentTimeM = date.getMinutes();
    let currentTimeS = date.getSeconds();

    let currentTime = currentTimeH + ":" + currentTimeM + ":" + currentTimeS;

    let launchDate = new Date(state.launchDate + " " + currentTime);
    let startDate = new Date(state.exchangeStart + " " + currentTime);

    if (startDate.getDay() !== 1 && launchDate.getDay() === 1) {
      swal("", "Start date must be a Monday", "error", {
        buttons: false,
      });
    } else if (launchDate.getDay() !== 1 && startDate.getDay() === 1) {
      swal("", "Launch date must be a Monday", "error", {
        buttons: false,
      });
    } else if (launchDate.getDay() !== 1 && startDate.getDay() !== 1) {
      swal("", "Launch and Start date must be a Monday", "error", {
        buttons: false,
      });
    } else {
      swal("Are you sure you want to do this?", {
        buttons: {
          cancel: "Cancel",
          yes: {
            text: "Yes",
            value: "yes",
          },
        },
      }).then((value) => {
        switch (value) {
          case "yes":
            // swal("Pikachu fainted! You gained 500 XP!");
            state.launchDate = launchDate.toISOString();
            state.exchangeStart = startDate.toISOString();
            updateDetails();
            break;

          default:
            console.log("Cancel");
        }
      });
    }
  };

  return (
    <div className="container mt-3">
      <div className="row mb-4">
        <div className="col-lg-8 mx-auto text-center">
          <h1 className="display-8">Warranty SKU Data</h1>
          <h6>
            Please adjust the OS, Tier, Start Date, Launch Date and/or is Active
            accordingly
          </h6>
        </div>
      </div>

      <Card
        bg="light"
        key="light"
        text="dark"
        // style={{ height: "27rem", overflow: "scroll", overflowX: "hidden" }}
        className="mb-2 p-3 save-sku-card"
      >
        <Form>
          <Form.Group className="mb-3">
            <Form.Label
              onClick={() => navigate("/warrantysku")}
              style={{ cursor: "pointer" }}
            >
              <FontAwesomeIcon icon={faArrowLeftLong} />
            </Form.Label>
          </Form.Group>

          {isNewSku ? (
            <h5>This is a new XSKU</h5>
          ) : (
            <h5>This is an existing XSKU</h5>
          )}

          {/* Make */}
          <Form.Group className="mb-3">
            <Form.Label>Make</Form.Label>
            <Form.Control
              defaultValue={state.make}
              disabled
              type="text"
              placeholder=""
            />
          </Form.Group>

          {/* Model */}
          <Form.Group className="mb-3">
            <Form.Label>Model</Form.Label>
            <Form.Control
              defaultValue={state.model}
              disabled
              type="text"
              placeholder=""
            />
          </Form.Group>

          {/* SKU */}
          <Form.Group className="mb-3">
            <Form.Label>SKU</Form.Label>
            <Form.Control
              defaultValue={state.sku}
              disabled
              type="text"
              placeholder=""
            />
          </Form.Group>

          {/* Operating system */}
          <Form.Group className="mb-3">
            <Form.Label htmlFor="select">Operating System</Form.Label>
            <Form.Select
              name="operatingSystem"
              onChange={(e) => handleChange(e)}
              value={state.operatingSystem}
            >
              {config.OPERATING_SYS.map((os, i) => {
                return <option key={i}>{os}</option>;
              })}
            </Form.Select>
          </Form.Group>

          {/* Tier */}
          <Form.Group className="mb-3">
            <Form.Label htmlFor="select">Tier</Form.Label>
            <Form.Select
              name="tierId"
              onChange={(e) => handleChange(e)}
              value={state.tierId}
            >
              {config.TIER_NAMES.map((tier, i) => {
                return <option key={i}>{tier}</option>;
              })}
            </Form.Select>
          </Form.Group>

          {/* Launch date */}
          <Form.Group className="mb-3">
            <Form.Label htmlFor="select">Launch Date</Form.Label>
            <Form.Control
              onChange={(e) => handleChange(e)}
              value={state.launchDate}
              name="launchDate"
              type="date"
            ></Form.Control>
          </Form.Group>

          {/* Exchange start date */}
          <Form.Group className="mb-3">
            <Form.Label htmlFor="select">Exchange Start Date</Form.Label>
            <Form.Control
              onChange={(e) => handleChange(e)}
              value={state.exchangeStart}
              name="exchangeStart"
              type="date"
            ></Form.Control>
          </Form.Group>

          {/* Active Report 1 */}
          <div className="form-check mb-4">
            <input
              className="form-check-input"
              type="checkbox"
              // value= {state.activeReport}
              id="flexCheckChecked"
              checked={state.activeReport}
              name="activeReport"
              onChange={(e) => handleCheckbox(e)}
            />
            <label className="form-check-label" htmlFor="flexCheckChecked">
              Exchange Analysis - is Active
            </label>
          </div>

          {/* Active Report 2 */}
          <div className="form-check mb-4">
            <input
              className="form-check-input"
              type="checkbox"
              // value= {state.activeReportTwo}
              id="flexCheckChecked"
              checked={state.activeReportTwo}
              name="activeReportTwo"
              onChange={(e) => handleCheckbox(e)}
            />
            <label className="form-check-label" htmlFor="flexCheckChecked">
              Planning Sheets - is Active
            </label>
          </div>

          {/* Forecast */}
          <div className="form-check mb-4">
            <input
              className="form-check-input"
              type="checkbox"
              // value= {state.oemRepairForecast}
              id="flexCheckChecked"
              checked={state.oemRepairForecast}
              name="oemRepairForecast"
              onChange={(e) => handleCheckbox(e)}
            />
            <label className="form-check-label" htmlFor="flexCheckChecked">
              Planning Sheets - OEM Repair Forecast
            </label>
          </div>

          <Button onClick={() => handleSubmit()} className="button-dark">
            Save
          </Button>
        </Form>
      </Card>
    </div>
  );
}

export default SaveSku;
