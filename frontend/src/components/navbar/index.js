import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

function NavbarApp() {
  const navigate = useNavigate();
  return (
    <Navbar sticky="top" variant="light">
      <Container fluid="lg">
        <Navbar.Brand href="/warrantysku">
          <img
            style={{ maxHeight: "2.8rem" }}
            src="https://www.t-mobile.com/content/dam/t-mobile/ntm/branding/logos/corporate/tmo-logo-v4.svg"
            alt="T-mobile"
          />
        </Navbar.Brand>
        <Navbar.Brand style={{ fontWeight: "600" }} href="/warrantysku">
          Warranty SKU Manager
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link className="me-3" onClick={() => navigate("/warrantysku/preview/sku")}>Preview</Nav.Link>
          <Nav.Link onClick={() => window.open("/apps-tools")}>All Apps and Tools</Nav.Link>
        </Nav>

        <InputGroup className="navbar-search">
          <Form.Control
            aria-label="Default"
            aria-describedby="inputGroup-sizing-default"
            placeholder="Search..."
            id="myInput"
            style={{
              visibility: window.location.pathname.includes("preview/sku")
                ? "visible"
                : "hidden",
            }}
          />
          <InputGroup.Text
            style={{
              visibility: window.location.pathname.includes("preview/sku")
                ? "visible"
                : "hidden",
            }}
            className="search-icon"
          >
            <FontAwesomeIcon size="1x" icon={faSearch} />
          </InputGroup.Text>
        </InputGroup>
      </Container>
    </Navbar>
  );
}

export default NavbarApp;
