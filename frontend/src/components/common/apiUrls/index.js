import config from "../config"

export const lookupSkuUrl = config.API_BASE + "find/sku/details/{{sku}}";

export const fetchXSKUUrl = config.API_BASE + "preview/xsku/list";

export const updateSKUDetailsURL = config.API_BASE + "update/xsku";

export const insertSKUDetailsURL = config.API_BASE + "insert/xsku";