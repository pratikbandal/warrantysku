import { fetchXSKUUrl, insertSKUDetailsURL, lookupSkuUrl, updateSKUDetailsURL } from "../apiUrls";
import config from "../config";
import core from "../core";

let app = {};

app.lookupSku = (sku) => {
  let params = {
    url: lookupSkuUrl.replace("{{sku}}", sku),
    method: "get",
    loader: true,
  };

  return core.makeAPICall(params);
};

app.getTierName = (tierId) => {
  const tierName = {
    11: "SMART HIGH",
    3: "OTHER",
    8: "BASIC",
    10: "SMART BASIC",
    9: "MBB",
    12: "TABLET",
    2: "ESSENTIALS",
  };

  return tierName[tierId];
};

app.getTierId = (tierId) => {
  const tierName = {
    "SMART HIGH": 11,
    OTHER: 2,
    BASIC: 8,
    "SMART BASIC": 10,
    MBB: 9,
    TABLET: 12,
    ESSENTIALS: 2,
  };

  return tierName[tierId];
};

app.getDate = (dateISO) => {
  if (dateISO) {
    let date = new Date(dateISO);
    return (
      date.getFullYear() +
      "-" +
      (date.getMonth() + 1).toString().padStart(2, "0") +
      "-" +
      date.getDate().toString().padStart(2, "0")
    );
  } else {
    return "NULL";
  }
};

app.fetchXSKUList = () => {
  let params = {
    url: fetchXSKUUrl,
    method: "get",
  };

  return core.makeAPICall(params);
};

app.sortTableHead = (skuHeads) => {
  let skuArray = [];

  let tableHead = config.SKU_LIST_HEAD;

  for (let name of tableHead) {
    skuHeads.forEach((skh) => {
      if (skh === name) {
        skuArray.push(skh);
      }
    });
  }
  return skuArray;
};

app.updateSKUDetails = (payload) => {
  let params = {
    url: updateSKUDetailsURL,
    method: "put",
    data: payload,
  };

  return core.makeAPICall(params);
};

app.insertSKUDetails = (payload) => {
  let params = {
    url: insertSKUDetailsURL,
    method: "post",
    data: payload,
  };

  return core.makeAPICall(params);
};

export default app;
