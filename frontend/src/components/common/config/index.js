let config = {};
config.URL_BASE = "/";

config.enumStaticUrls = {
  home: "/",
};

// config.BASE_DOMAIN = "http://localhost:3009/";
// config.API_BASE = config.BASE_DOMAIN + "warranty/manager/api/";

// Development env.
if (window.location.hostname.includes("localhost")) {
  config.BASE_DOMAIN = "http://localhost:3011/";
  config.API_BASE = config.BASE_DOMAIN + "warranty/manager/api/";
  console.log("dev")
}
// Production env.
else {
  config.BASE_DOMAIN = "http://10.77.103.136:3009/";
  config.API_BASE = config.BASE_DOMAIN + "warranty/manager/api/";
}

config.TIER_NAMES = [
  "OTHER",
  "BASIC",
  "MBB",
  "SMART BASIC",
  "SMART HIGH",
  "TABLET",
];
config.OPERATING_SYS = ["OTHER", "WINDOWS", "ANDROID", "iOS", "BLACKBERRY"];
config.SKU_LIST_HEAD = [
  "XSKU",
  "OperatingSystem",
  "TierID",
  "ActiveReport",
  "ActiveReport2",
  "OEMRepairForecast",
  "LaunchDate",
  "ExchangeStart",
  "InsertDate",
  "UserID",
];

config.FILTER_GROUP = ["OperatingSystem", "TierID"]

export default config;
