import React, { useState } from "react";
import { Card, InputGroup, Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useNavigate } from "react-router-dom";
import app from "../common/app";
import swal from "sweetalert"

const Home = () => {
  const [sku, setSku] = useState("");
  const navigate = useNavigate();

  const handleChange = (e) => {
    setSku(e.target.value);
  };

  const handleSubmit = async () => {
    const resp = await app.lookupSku(sku);

    if (resp.data.meta && resp.data.meta.retval === 1) {
      navigate(`/warrantysku/save-sku?sku=${sku}`, {
        state: { skuDetails: resp.data.data, isNewSku: resp.data.isNewSku },
      });
    } else if (resp.data.meta.retval === 2) {
      swal("SKU not found", "", "error")
    }
  };
  return (
    <div className="home-container mt-5">
      <Card
        bg="light"
        key="light"
        text="dark"
        style={{ width: "28rem" }}
        className="mb-2"
      >
        <Card.Header style={{ fontWeight: "700" }} className="display-8">
          Warranty SKU Data
        </Card.Header>
        <Card.Body>
          <InputGroup className="mb-3">
            <InputGroup.Text
              id="inputGroup-sizing-default"
              className="label-input"
            >
              Warranty SKU
            </InputGroup.Text>
            <Form.Control
              aria-label="Default"
              aria-describedby="inputGroup-sizing-default"
              type="text"
              onChange={(e) => handleChange(e)}
              value={sku}
            />
          </InputGroup>
          <Card.Text>
            <Button
              disabled={sku ? false : true}
              onClick={() => handleSubmit()}
              className="button-dark"
            >
              Lookup SKU
            </Button>
          </Card.Text>
          <hr />
          <Card.Text>
            <Button onClick= {() => navigate("/warrantysku/preview/sku")} className="button-light">Preview</Button>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Home;
