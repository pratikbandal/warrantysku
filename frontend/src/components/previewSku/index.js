import React, { useEffect, useState } from "react";
import { Card, Dropdown, Placeholder, Table } from "react-bootstrap";
import app from "../common/app";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import config from "../common/config";
import $ from "jquery";

function PreviewSku() {
  const [skuList, setSkuList] = useState([]);

  useEffect(() => {
    let containerFluidEl = document.querySelector(".container-fluid");
    let btnEl = document.querySelector(".navbar-nav a");
    if (containerFluidEl && btnEl) {
      containerFluidEl.style.backgroundColor = "white";
      btnEl.style.backgroundColor = "#e20074";
      btnEl.style.setProperty("color", "white", "important");
      btnEl.style.setProperty("border-color", "transparent", "important");
    }
    const fetchXSKUList = async () => {
      const resp = await app.fetchXSKUList();
      if (resp?.data.meta.retval === 1) {
        setSkuList(resp.data.data);
      }
    };

    fetchXSKUList();

    return () => {
      //
    };
  }, []);

  const filterTable = (value) => {
    if (value.toLowerCase() !== "basic") {
      $("#myTable tr").filter(function () {
        $(this).toggle(
          $(this).text().toLowerCase().indexOf(value.toLowerCase()) > -1
        );
        return null;
      });
    } else {
      $("#myTable tr").filter(function () {
        $(this).toggle(
          $(this).text().toLowerCase().indexOf(value.toLowerCase()) > -1 &&
            $(this).text().toLowerCase().indexOf("smart") < 0
        );
        return null;
      });
    }
  };

  let tableHead = [];
  if (skuList?.length) {
    tableHead = app.sortTableHead(Object.keys(skuList[0]));
  }

  return (
    // skuList?.length && tableHead?.length ?
    <div className="preview-table mt-5">
      <Table bordered striped responsive="sm" hover size="md" variant="light">
        <thead style={{ position: "sticky", top: 0, zIndex: "1111" }}>
          <tr>
            {tableHead?.length ? (
              tableHead.map((tbh, i) => {
                tbh = tbh === "ActiveReport" ? "Exchange Analysis" : tbh;

                return (
                  <th key={i}>
                    {tbh}
                    {config.FILTER_GROUP.includes(tbh) && (
                      <Dropdown className="dropdown filter-dropdown">
                        <Dropdown.Toggle
                          size="sm"
                          variant="success"
                          id="dropdown-basic"
                        >
                          <FontAwesomeIcon size="1x" icon={faFilter} />
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                          {config[
                            tbh === "OperatingSystem"
                              ? "OPERATING_SYS"
                              : "TIER_NAMES"
                          ].map((os, i) => {
                            if (os === "OTHER") {
                              return null;
                            }
                            return (
                              <Dropdown.Item
                                key={i}
                                onClick={() => filterTable(os)}
                              >
                                {os}
                              </Dropdown.Item>
                            );
                          })}
                        </Dropdown.Menu>
                      </Dropdown>
                    )}
                  </th>
                );
              })
            ) : (
              <th>
                <Placeholder as={Card.Title} animation="glow">
                  <Placeholder xs={6} />
                </Placeholder>
              </th>
            )}
          </tr>
        </thead>
        <tbody id="myTable">
          {skuList?.length
            ? skuList.map((sku, i) => {
                return (
                  <tr key={sku.XSKU}>
                    <td>{sku.XSKU}</td>
                    <td>{sku.OperatingSystem}</td>
                    <td>{app.getTierName(sku.TierID)}</td>
                    <td>{sku.ActiveReport ? "Active" : "Deactive"}</td>
                    <td>{sku.ActiveReport2 ? "Active" : "Deactive"}</td>
                    <td>{sku.OEMRepairForecast ? "Active" : "Deactive"}</td>
                    <td>{app.getDate(sku.ExchangeStart)}</td>
                    <td>{app.getDate(sku.LaunchDate)}</td>
                    <td>{app.getDate(sku.InsertDate)}</td>
                    <td>{sku.UserID}</td>
                  </tr>
                );
              })
            : [1, 2, 3, 4, 5, 6, 7, 8].map((el, i) => {
                return (
                  <tr key={i}>
                    <td>
                      <Placeholder as={Card.Title} animation="glow">
                        <Placeholder xs={6} />
                      </Placeholder>{" "}
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
    </div>
  );
}

export default PreviewSku;
