import React, { lazy } from "react";
import { Route, Routes } from "react-router-dom";
import { Loader } from "../loader";
import NavbarApp from "../navbar";

const Home = lazy(() => import(/* webpackChunkName: 'home' */ "../home"));
const SaveSku = lazy(() =>
  import(/* webpackChunkName: 'saveSku' */ "../saveSku")
);
const PreviewSku = lazy(() => import(/* webpackChunkName: 'previewSku' */'../previewSku'));
const Login = lazy(() => import(/* webpackChunkName: 'previewSku' */'../login'));

const AppRoutes = () => {

  return (
    <>
      <NavbarApp />

      <Routes>
        <Route
          path="/warrantysku"
          key={0}
          element={
            <React.Suspense fallback={<Loader />}>
              <Home />
            </React.Suspense>
          }
        />
        <Route
          path="/warrantysku/save-sku"
          key={1}
          element={
            <React.Suspense fallback={<Loader />}>
              <SaveSku />
            </React.Suspense>
          }
        />
        <Route
          path="/warrantysku/preview/sku"
          key={2}
          element={
            <React.Suspense fallback={<Loader />}>
              <PreviewSku />
            </React.Suspense>
          }
        />
        <Route
          path="/login"
          key={3}
          element={
            <React.Suspense fallback={<Loader />}>
              <Login />
            </React.Suspense>
          }
        />
      </Routes>
      <div className="lds-default mu-hidden">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </>
  );
};

export default AppRoutes;
